﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCorePractice.Context
{
    public class FormAssocciates
    {
        public int Id { get; set; }

        public string Name { get; set; } = default!;

        public int? FormId { get; set; }

        public Form? Form { get; set; }

        public int? ApplicationUserId { get; set; }

        public ApplicationUser ApplicationUser { get; set; }
    }

    public class Form
    {
        public int Id { get; set; }

        public string Name { get; set; } = default!;

        public int? ApplicationUserId { get; set; }

        public ApplicationUser ApplicationUser { get; set; }
    }

    public class ApplicationUser
    {
        public int Id { get; set; }

        public string Firstname { get; set; } = default!;

        public string Surname { get; set; } = default!;
    }

    public class AppDbContext : DbContext
    {
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }

        public DbSet<Form> Forms { get; set; }

        public DbSet<FormAssocciates> FormAssocciates { get; set; }

        public AppDbContext(DbContextOptions options)
            : base(options)
        {
        }
    }
}
