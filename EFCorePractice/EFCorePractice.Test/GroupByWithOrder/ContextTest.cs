﻿using EFCorePractice.Context;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace EFCorePractice.Test.GroupByWithOrder
{
    [TestFixture]
    public class ContextTest
    {
        [Test]
        public void shouldThrowException()
        {
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseSqlite("Data Source=GroupBy.db");

            using var context = new AppDbContext(optionsBuilder.Options);

            string queryWithoutOrderBy = context.FormAssocciates
                              .Include(x => x.ApplicationUser)
                              .Include(x => x.Form).ThenInclude(x => x.ApplicationUser)
                              .GroupBy(x => x.FormId)
                              .Select(x => x.First().Form)
                              .ToQueryString();

            Console.WriteLine(queryWithoutOrderBy);

            //To się wywala:
            context.Invoking(x => x.FormAssocciates
                              .Include(x => x.ApplicationUser)
                              .Include(x => x.Form).ThenInclude(x => x.ApplicationUser)
                              .GroupBy(x => x.FormId)
                              .Select(x => x.First().Form)
                              .OrderBy(x => x.Name)
                              .Skip(20)
                              .Take(10).ToQueryString()).Should().Throw<Exception>();
        }
    }
}
